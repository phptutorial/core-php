<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    
    <form action="associativeArray.php" method="POST">
        <input type="text" name="txt_name" />
        <input type="submit"/>
    </form>

    <!-- Key Value Pair -->
    <?php
        $key = $_POST["txt_name"];
    // Jim = key, a+ = value
        $grades = array("jim" => "A+", "Pam" => "B-", "Oscar" => "C+");

        echo ($grades[$key]);

    ?>

</body>
</html>