<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <form action="switchcase.php" method="POST">

        <input type="text" name="txt_grade"/><br/>
        <input type="submit"/>

    </form>

    <?php 
        $grade = $_POST["txt_grade"];
        
        switch($grade){

            case "A" :
                echo "Excellent Job";
                break;

            case "B" :
                echo "Good Grades";
                break;
            
            case "C":
                echo "Could do better";
                break;

            case "D":
                echo "Poor Performance";
                break;

            case "F":
                echo "You failed";
                break;

            default:
                echo "Invalid Grade";
                break;          }
    ?>
</body>
</html>