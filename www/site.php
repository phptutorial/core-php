<!DOCTYPE html>
    <head>
        <title>PHP</title>
    </head>
    <body>
        <?php

            $characterName = "john";
            $characterAge = 36;

            echo "There was once a man named $characterName <br/>";
            echo "He was $characterAge years old";
        ?>
    </body>
</html>