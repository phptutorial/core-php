<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    
    <form action="madlibs.php" method="GET">
        
        <input type="text" placeholder="Enter a colour" name="color"/>
        <br/>

        <input type="text" placeholder="Enter a plural noun" name="pluralNoun" />
        <br/>
        
        <input type="text" placeholder="Enter a celebrity" name="celebrity" />
        <br/>

        <input type="submit"/>

    </form>

    <?php 
        $color = $_GET['color'];
        $pluralNoun = $_GET['pluralNoun'];
        $celebrity = $_GET['celebrity'];

        echo "Roses are $color <br/>";
    
        echo "$pluralNoun are blue <br/>";
        
        echo "I love $celebrity ";

    ?>

</body>
</html>