<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
    // Arrays in php can store any data type
        $friendsList = array('kevin', 'tomba', true, 25);

        echo "$friendsList[0] <br/>";
        echo "$friendsList[1] <br/>";
        echo "$friendsList[2] <br/>";
        echo "$friendsList[3] <br/>";

    ?>
</body>
</html>