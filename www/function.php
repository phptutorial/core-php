<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    
    <?php
        function sayHi(){
            echo ("<p> Hello User </p>");
        };

        function sayHiWithName($name){
            echo "<p> Hi $name </p>";
        };

        function cube($num){
            return $num * $num * $num;
        }

        sayHi();

        sayHiWithName("Sid");

        $cuboid = cube(3);
        echo "<p> Cube of 3 = $cuboid </p>";

    ?>

</body>
</html>